---
title: Legal
description: ''
background_color: ''
children:
- template: resources
  title: Other master lists
  children:
  - template: resource
    link: https://transequality.org/know-your-rights
    label: Know yours rights
    description: This seems to be a mostly US-centered resource, but could still be
      useful.
  description: ''
  background_color: ''

---
