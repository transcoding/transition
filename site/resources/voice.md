---
title: Voice
children:
- template: resources
  title: Tools
  children:
  - template: resource
    label: What is Your Voice Gender?
    link: https://voicegender.herokuapp.com/
    description: Use ML to figure out gender of a voice.
  - template: resources
    title: Android Apps
    children:
    - template: resource
      link: https://play.google.com/store/apps/details?id=com.DevExtras.VoiceTools
      label: Voice tools by DevExtras
      description: ''
    - template: resource
      link: https://play.google.com/store/apps/details?id=de.lilithwittmann.voicepitchanalyzer
      label: Voice Pitch Analyzer by Purr Programming
      description: ''
    description: ''
    background_color: ''
  description: ''
  background_color: ''
- template: resources
  title: Guides
  description: ''
  children:
  - template: resource
    link: https://www.reddit.com/r/transvoice/comments/b340gq/hows_my_girl_voice_after_eight_weeks_3_im/ej2uyal
    label: Lsomethingsomething's indepth guide
    description: ''
  background_color: ''
- template: resources
  title: Other master lists
  children:
  - template: resource
    link: https://www.reddit.com/r/transvoice/wiki/index
    label: "/r/transvoice wiki"
    description: ''
  description: ''
  background_color: ''
- template: resources
  title: Communities
  children:
  - template: resource
    link: https://discord.gg/v42vvyF
    label: Scinguistics Discord
    description: ''
  description: ''
  background_color: ''
description: ''
background_color: ''

---
