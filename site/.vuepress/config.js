const config = require('../config.json');
module.exports = {
  title: config.title,
  description: config.description,
  base: "/",
  themeConfig: {
    logo: config.logo,
    footer: config.footer,
    nav: config.navigation,
  },
  head: [
    ['link', { rel: "icon", href: config.favicon }],
    ['link', {rel: 'stylesheet', href: 'https://cdn.jsdelivr.net/npm/bulma@0.7.5/css/bulma.min.css'}],
    ['link', {rel: 'stylesheet', href: 'https://cdn.jsdelivr.net/npm/bulma-timeline@3.0.4/dist/css/bulma-timeline.min.css'}],
  ],
  markdown: {
    anchor: {
      permalink: false
    }
  }
};
