---
date: 1/10/2017
title: Relationship that started the crack

---
This was my first real relationship, one that resulted in me starting to question my gender.
It didn't last the longest, but I did enjoy it the most at the time, and I am very thankful to her.