---
hero_text: Just your friendly trans girl telling her tales.
title: Home

---
<Pages-Hero :text="$page.frontmatter.hero_text" />
<List-Journal />